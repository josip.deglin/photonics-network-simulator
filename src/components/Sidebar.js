import React from 'react';
import { useStoreState, useStoreActions, getConnectedEdges } from 'react-flow-renderer';
export default () => {
    const nodes = useStoreState((store) => store.nodes);
    const edges = useStoreState((store) => store.edges);

    const setSelectedElements = useStoreActions((actions) => actions.setSelectedElements);
    const selectAll = () => {
        setSelectedElements(nodes.map((node) => ({ id: node.id, type: node.type })));
        console.log(nodes);
    };
    const clickMe = () => {
        alert('Yuu clicked me')
        // ispis samo veza:
        //console.log(edges);

        //ispis čvorova i veza 
        console.log(JSON.stringify(nodes) + JSON.stringify(edges));
    };

    const onDragStart = (event, nodeType) => {
        event.dataTransfer.setData('application/reactflow', nodeType);
        event.dataTransfer.effectAllowed = 'move';
    };
    return (
        <aside>
            <div className="description">
                This is an example of how you can access the internal state outside of the ReactFlow component.
      </div>

            <div className="title">Nodes</div>
            {nodes.map((node) => (
                <div key={node.id}>
                    Node {node.id} - x: {node.__rf.position.x.toFixed(2)}, y: {node.__rf.position.y.toFixed(2)}
                </div>
            ))}
            <div className="selectall">
                <button onClick={selectAll}>select all nodes</button>
            </div>
            <br></br>
            <button onClick={clickMe}>
                JSON-console
            </button>
            <div className="description">You can drag these nodes to the pane on the right.</div>
            <div className="dndnode input" onDragStart={(event) => onDragStart(event, 'transmitter')} draggable>
                Predajnik
             </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'amplifier')} draggable>
                Pojačalo
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'converter')} draggable>
                Valni pretvornik
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'coupler')} draggable>
                Sprežnik
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'spreader')} draggable>
                Rasprežnik
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'filter')} draggable>
                Filter
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'mux')} draggable>
                Mux 4:1
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'demux')} draggable>
                Demux 1:4
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'oadm')} draggable>
                Mux s dodavanjem/izuzimanjem
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'switch')} draggable>
                Prospojnik
            </div>
            <div className="dndnode output" onDragStart={(event) => onDragStart(event, 'receiver')} draggable>
                Prijemnik
            </div>
            <div className="dndnode output" onDragStart={(event) => onDragStart(event, 'fiber')} draggable>
                Optičko vlakno
            </div>
        </aside>
    );
};