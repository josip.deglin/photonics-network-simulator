import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#BACBA9',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    // const [signalAmplification, updateSignalAmplification] = useState(data.item.signalAmplification);
    // data.item.signalAmplification = signalAmplification;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Valni pretvornik</div>
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                Ulazna valna duljina: <AutosizeInput type='text' name="inputSignalWavelength" size="2" value={item.inputSignalWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
            <br />
                Izlazna valna duljina: <AutosizeInput type='text' name="outputSignalWavelength" size="2" value={item.outputSignalWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
            <br />



            <Handle
                type="target"
                position="left"
            />
            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
