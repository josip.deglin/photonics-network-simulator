import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#9CA8B3',
    color: '#FFF',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Fiber</div>
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                duljina: <AutosizeInput type='text' name="length" size="2" value={item.length} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                alpha:  <AutosizeInput type='text' name="alpha" size="2" value={item.alpha} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />


            <Handle
                type="target"
                position="left"
            />
            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
