import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#F7CE76',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            <div>Demux</div>
            <h1>1:4</h1>
            <br></br>
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                1: <AutosizeInput className="form__field" type='text' name="outputOneLowerLimitWavelength" size="2" value={item.outputOneLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="outputOneUpperLimitWavelength" size="2" value={item.outputOneUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                2: <AutosizeInput className="form__field" type='text' name="outputTwoLowerLimitWavelength" size="2" value={item.outputTwoLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="outputTwoUpperLimitWavelength" size="2" value={item.outputTwoUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                3:  <AutosizeInput className="form__field" type='text' name="outputThreeLowerLimitWavelength" size="2" value={item.outputThreeLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="outputThreeUpperLimitWavelength" size="2" value={item.outputThreeUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                4:  <AutosizeInput className="form__field" type='text' name="outputFourLowerLimitWavelength" size="2" value={item.outputFourLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="outputFourUpperLimitWavelength" size="2" value={item.outputFourUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

            <Handle
                type="target"
                position="left"
            />
            <Handle
                type="source"
                position="right"
                id="a"
                style={{ top: '20%' }}
            />
            <Handle
                type="source"
                position="right"
                id="b"
                style={{ top: '40%' }}
            />
            <Handle
                type="source"
                position="right"
                id="c"
                style={{ top: '60%' }}
            />
            <Handle
                type="source"
                position="right"
                id="d"
                style={{ top: '80%' }}
            />
        </div>
    );
});
