
import axios from "axios";

const baseURL = 'http://localhost:8080/photonic-network-simulator/api/add-drop-multiplexer/';


export async function getAllAddDropMultiplexers() {
    try {
        const response = await axios({
            method: 'get',
            baseURL: baseURL + 'all'
        });
        console.log(response.data);
        return response.data;
    } catch (e) {
        console.log(e)
    }
}
