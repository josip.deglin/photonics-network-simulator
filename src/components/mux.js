import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#F27348',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            <div>Mux</div>
            <h1>4:1</h1>
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                1: <AutosizeInput className="form__field" type='text' name="inputOneLowerLimitWavelength" size="2" value={item.inputOneLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="inputOneUpperLimitWavelength" size="2" value={item.inputOneUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                2: <AutosizeInput className="form__field" type='text' name="inputTwoLowerLimitWavelength" size="2" value={item.inputTwoLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="inputTwoUpperLimitWavelength" size="2" value={item.inputTwoUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                3:  <AutosizeInput className="form__field" type='text' name="inputThreeLowerLimitWavelength" size="2" value={item.inputThreeLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="inputThreeUpperLimitWavelength" size="2" value={item.inputThreeUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                4:  <AutosizeInput className="form__field" type='text' name="inputFourLowerLimitWavelength" size="2" value={item.inputFourLowerLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="inputFourUpperLimitWavelength" size="2" value={item.inputFourUpperLimitWavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

            <Handle
                type="source"
                position="right"
            />
            <Handle
                type="target"
                position="left"
                id="a"
                style={{ top: '20%' }}
            />
            <Handle
                type="target"
                position="left"
                id="b"
                style={{ top: '40%' }}
            />
            <Handle
                type="target"
                position="left"
                id="c"
                style={{ top: '60%' }}
            />
            <Handle
                type="target"
                position="left"
                id="d"
                style={{ top: '80%' }}
            />
        </div >
    );
});
