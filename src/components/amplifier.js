import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#E1F4CB',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Pojačalo</div>
            raspon: {data.item.lowerLimitWavelengthSpecter} - {data.item.upperLimitWavelengthSpecter} <br />
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                pojačanje: <AutosizeInput type='text' name="signalAmplification" size="2" value={item.signalAmplification} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
            <br />
                noiseFactor:<AutosizeInput type='text' name="noiseFactor" size="2" value={item.noiseFactor} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                saturationPower <AutosizeInput type='text' name="saturationPower" size="2" value={item.saturationPower} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />


            <Handle
                type="target"
                position="left"
            />
            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
