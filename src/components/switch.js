import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#F9968B',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Prospojnik</div>
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

                Relacija: <AutosizeInput type='text' name="switchType" size="2" value={item.switchType} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />



            <Handle
                type="target"
                position="left"
                id="a"
                style={{ top: '30%' }}
            />
            <Handle
                type="target"
                position="left"
                id="b"
                style={{ top: '70%' }}
            />
            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
