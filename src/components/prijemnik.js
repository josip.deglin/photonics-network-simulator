import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#c9daf9',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Prijemnik</div>
            raspon: {item.lowerWavelengthLimit} - {item.upperWavelengthLimit} <br />
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

            <Handle
                type="target"
                position="left"
            />
        </div>
    );
});
