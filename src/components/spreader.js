import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#FBECDB',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Rasprežnik</div>
            <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                Omjer rasprezanja α: <AutosizeInput type='text' name="dispersionRatio" size="2" value={item.dispersionRatio} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
            <br />
                Gubitak rasprezanja:<AutosizeInput type='text' name="splittingLost" size="2" value={item.splittingLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                Usmjerenost: <AutosizeInput type='text' name="orientation" size="2" value={item.orientation} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

            <Handle
                type="source"
                position="right"
                id="a"
                style={{ top: '30%' }}
            />
            <Handle
                type="source"
                position="right"
                id="b"
                style={{ top: '70%' }}
            />
            <Handle
                type="target"
                position="left"
            />
        </div>
    );
});
