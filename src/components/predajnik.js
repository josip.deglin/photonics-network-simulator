import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#DEEDE6',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            <div>Predajnik</div>
            <input
                className="nodrag"
                type="color"
                // onChange={data.onChange}
                defaultValue={data.color}
            />
            <br />
            raspon valne dujine: <AutosizeInput className="form__field" type='text' name="lowerWavelengthLimit" size="2" value={item.lowerWavelengthLimit} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
             - <AutosizeInput className="form__field" type='text' name="upperWavelengthLimit" size="2" value={item.upperWavelengthLimit} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />  <br />
                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                valna duljina: <AutosizeInput className="form__field" type='text' name="wavelength" size="2" value={item.wavelength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                snaga <AutosizeInput className="form__field" type='text' name="strength" size="2" value={item.strength} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />

            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
