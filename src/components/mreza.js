import React, { useState, useRef, useEffect } from 'react';
import ReactFlow, { ReactFlowProvider, removeElements, addEdge, Background, Controls } from 'react-flow-renderer';

import axios from 'axios'
import './provider.css';

import Sidebar from './Sidebar';
import Predajnik from './predajnik';
import DeMux from './demux';
import Prijemnik from './prijemnik';
import Amplifier from './amplifier';
import Converter from './converter';
import Filter from './filter';
import Coupler from './coupler';
import Mux from './mux';
import Spreader from './spreader'
import Switch from './switch'
import OADM from './admux'
import Fiber from './fiber'



import Demo from './demo.json';

//import Start from './startDemo.json'


const nodeTypes = {
    transmitter: Predajnik,
    demux: DeMux,
    receiver: Prijemnik,
    amplifier: Amplifier,
    converter: Converter,
    coupler: Coupler,
    filter: Filter,
    mux: Mux,
    spreader: Spreader,
    switch: Switch,
    oadm: OADM,
    fiber: Fiber

};
const clearElements = [];
const demoElements = Demo;
//const startElements = Start;
var item = [];

let id = 0;
const getId = () => `dndnode_${id++}`;

const Mreza = () => {
    //spremanje podataka iz apia
    const [items, setItems] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    //ReactFlow elementi
    const reactFlowWrapper = useRef(null);
    const [reactFlowInstance, setReactFlowInstance] = useState(null);
    const [elements, setElements] = useState(clearElements);

    // trenutno implementirani modeli
    const modeli = ["transmitter", "demultiplexer", "receiver", "amplifier", "converter", "coupler", "filter", "multiplexer", "spreader", "switch", "add-drop-multiplexer", "fiber"];

    useEffect(() => {
        const fetchItems = async (model) => {
            setIsLoading(true)
            const result = await axios(
                `http://localhost:8080/photonic-network-simulator/api/${model}/1`
            )
            setItems((es) => es.concat(result.data));
            setIsLoading(false)
        }
        modeli.map((model) => fetchItems(model))
    }, [])

    const onElementsRemove = (elementsToRemove) =>
        setElements((els) => removeElements(elementsToRemove, els));

    // trenutno duljina i alpha dodani kao labela ali moguće je dodati kao i parms.data 
    // no onda treba napraviti i naš tip veze koji će priazivati npr <p> params.data.alpha </p>
    const onConnect = (params) => {
        params.type = 'smoothstep';
        params.animated = true;
        // params.label = "alpha:0.2 Lenght =10"
        setElements((els) => addEdge(params, els));
    }


    const loadDemo = () => {
        setElements(demoElements);
    }
    // const startDemo = () => {
    //     setElements(startElements);
    // }
    const clearNet = () => {
        setElements(clearElements);
    }

    const onLoad = (_reactFlowInstance) =>
        setReactFlowInstance(_reactFlowInstance);
    const onDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
    };
    const onDrop = (event) => {
        event.preventDefault();
        const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
        const type = event.dataTransfer.getData('application/reactflow');
        const position = reactFlowInstance.project({
            x: event.clientX - reactFlowBounds.left,
            y: event.clientY - reactFlowBounds.top,
        });
        items.map((i) => {
            //console.log(i);
            if (i.type === type.toUpperCase()) {
                //console.log(type.toUpperCase());
                item = i;
            }
        });
        const newNode = {
            id: getId(),
            type,
            position,
            data: { label: `${type} node`, item },
        };
        // , text: oadm
        setElements((es) => es.concat(newNode));
    };

    return isLoading ? <h1>Loading...</h1> : (
        <>
            <div className="providerflow" >
                <ReactFlowProvider>
                    <div className="reactflow-wrapper" ref={reactFlowWrapper}>
                        <ReactFlow
                            elements={elements}
                            onLoad={onLoad}
                            onDrop={onDrop}
                            onDragOver={onDragOver}
                            onElementsRemove={onElementsRemove}
                            style={{ width: '100%', height: '90vh' }}
                            onConnect={onConnect}
                            nodeTypes={nodeTypes}
                            deleteKeyCode={46} /* 'delete'-key */
                            snapToGrid={true}
                            snapGrid={[15, 15]}
                        >
                            <Controls />
                            <Background color="#aaa" gap={16} />
                        </ReactFlow>
                    </div>
                    <Sidebar />
                </ReactFlowProvider>
            </div>
            <div>

                <br></br>
                <button
                    type="button"
                    onClick={loadDemo}
                >Load Demo</button>
                {/* <button
                    type="button"
                    onClick={startDemo}
                >Start Demo</button> */}
                <button
                    type="button"
                    onClick={clearNet}
                >Clear all</button>
            </div>
        </>
    );
}
export default Mreza