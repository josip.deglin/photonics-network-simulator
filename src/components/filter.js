import React, { memo, useState } from 'react';
import { Handle } from 'react-flow-renderer';
import AutosizeInput from 'react-input-autosize';

const customNodeStyles = {
    background: '#98D4BB',
    color: '#000',
    padding: 10,
};
export default memo(({ data }) => {
    const [item, changeItem] = useState(data.item);
    data.item = item;
    return (
        <div style={customNodeStyles}>
            {/* <div>{data.text}</div> */}
            <div>Filter</div>
            <br />

                IL: <AutosizeInput className="form__field" type='text' name="insertionLost" size="2" value={item.insertionLost} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} /> <br />
                raspon: <AutosizeInput className="form__field" type='text' name="lowerWavelengthLimit" size="2" value={item.lowerWavelengthLimit} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />
                  -   <AutosizeInput className="form__field" type='text' name="upperWavelengthLimit" size="2" value={item.upperWavelengthLimit} onChange={(e) => changeItem({ ...item, [e.target.name]: e.target.value })} />

            <Handle
                type="target"
                position="left"
            />
            <Handle
                type="source"
                position="right"
            />
        </div>
    );
});
